// ArbolBinario.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
#include"ArbolBB.h"
using namespace std;

int main()
{
    ArbolBB arbolBinario;
    arbolBinario.Raiz = &Nodo(10);
    arbolBinario.Raiz->Der = &Nodo(15);
    arbolBinario.Raiz->Der->Der = &Nodo(25);
    arbolBinario.Raiz->Izq = &Nodo(5);
    cout << arbolBinario.Raiz->grado() << endl;
    cout << arbolBinario.Raiz->Der->Der->grado() << endl;
    std::cout << "Hello World!\n";

}
#include "Nodo.h"

Nodo::Nodo(int num)
{
	//Constructor de nuevos nodos
	Valor = num;
	Izq = nullptr;
	Der = nullptr;
}

bool Nodo::esHoja()
{
	return Izq == nullptr && Der == nullptr; 
}

bool Nodo::esInterno()
{
	return !esHoja();
}

int Nodo::grado()
{
	if (Izq == nullptr && Der == nullptr)
		return 0;
	else if (Izq != nullptr && Der != nullptr)
		return 2;
	else
		return 1;

}

#pragma once
#include <iostream>
#include<stdlib.h>
#include<string>
#include<fstream>
using namespace std;

void lectura();

int main()
{
    lectura();
    return 0;
}

void lectura(){
    ifstream archivo;
    string texto;

    archivo.open("textos.txt", ios::in); //apertura del archive en modo lectura
    if(archivo.fail()){
        cout <<"No se pudo abrir el archivo";
        exit(1);

    }
    while(!archivo.eof()){ //mientras no sea el final del archivo guardo el recorrido
        getline(archivo, texto);
        cout <<texto<<endl;
    }
    archivo.close(); //cerramos el archivo
}
